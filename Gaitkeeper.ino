#include "src/gaitkeeper.h"
#include "src/ble/ble.h"

void setup() {
  gaitkeeper__setup();
}

void loop() {
  gaitkeeper__loop();
}
