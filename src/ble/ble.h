#ifndef ble_h
#define ble_h

const uint8_t GDS_UUID_SERVICE [] = {
  0xFB, 0x34, 0x9B, 0x5F, 0x80, 0x00, 0x00, 0x80,
  0x00, 0x10, 0x00, 0x00, 0xAD, 0xDE, 0x00, 0x00
};

const uint8_t GDS_UUID_CHR_DCP [] = { //data control point characteristic 
  0xFB, 0x34, 0x9B, 0x5F, 0x80, 0x00, 0x00, 0x80,
  0x00, 0x10, 0x00, 0x00, 0xEF, 0xBE, 0x00, 0x00
};

const uint8_t GDS_UUID_CHR_ADB [] = {
  0xFB, 0x34, 0x9B, 0x5F, 0x80, 0x00, 0x00, 0x80,
  0x00, 0x10, 0x00, 0x00, 0xBA, 0xB0, 0x00, 0x00
};

const uint8_t GDS_UUID_CHR_DATA [] = {
  0xFB, 0x34, 0x9B, 0x5F, 0x80, 0x00, 0x00, 0x80,
  0x00, 0x10, 0x00, 0x00, 0xFE, 0xCA, 0x00, 0x00
};

#define GDS_DCP_START_TRANSFER_OPCODE 0x01 //not sure if arduino has enums, but not like anybody is ever gonna see this code anyway
#define GDS_DCP_PAUSE_TRANSFER_OPCODE 0x02 

#define GDS_DCP_OP_DEFAULT_RETCODE 0x00 //future use if required 
#define GDS_DCP_OP_SUCCESS_RETCODE 0x01 
#define GDS_DCP_OP_FAIL_RETCODE 0x02 

#define GDS_DCP_LEN 1 
#define GDS_ADB_LEN 4 
#define MAX_GDS_DATA_LEN 20 

void ble_gds_setup(); 
void ble_gds_loop(); 
void ble_stop_data_transfer(); 
void ble_begin_data_transfer(); 
void begin_data_transfer(uint16_t conn_handle); 

#endif 
