#include <bluefruit.h>
#include "ble.h"

#include <SD.h>

/* HRM Service Definitions
 * Heart Rate Monitor Service:  0x180D
 * Heart Rate Measurement Char: 0x2A37
 * Body Sensor Location Char:   0x2A38
 */
 
/* Gait Data Service (GDS) UUID       : 0000DEAD-0000-1000-8000-00805F9B34FB
 * GDS Data Control Point Char UUID   : 0000BEEF-0000-1000-8000-00805F9B34FB
 * GDS Available Data Bytes Char UUID : 0000B0BA-0000-1000-8000-00805F9B34FB
 * GDS Data Char UUID                 : 0000CAFE-0000-1000-8000-00805F9B34FB
 */

uint32_t available_data_len = 0; 
bool transfer_data = false; 

BLEService        hrms = BLEService(UUID16_SVC_HEART_RATE);
BLECharacteristic hrmc = BLECharacteristic(UUID16_CHR_HEART_RATE_MEASUREMENT);
BLECharacteristic bslc = BLECharacteristic(UUID16_CHR_BODY_SENSOR_LOCATION);

BLEService        gds = BLEService(GDS_UUID_SERVICE);
BLECharacteristic gds_dcp = BLECharacteristic(GDS_UUID_CHR_DCP);
BLECharacteristic gds_adb = BLECharacteristic(GDS_UUID_CHR_ADB); 
BLECharacteristic gds_data = BLECharacteristic(GDS_UUID_CHR_DATA); 

BLEDis bledis;    // DIS (Device Information Service) helper class instance
BLEBas blebas;    // BAS (Battery Service) helper class instance

uint8_t  bps = 0;

struct imuDataPayload_t {
  uint32_t us_time;
  uint16_t unix_time;
  uint16_t x_accel;
  uint16_t y_accel;
  uint16_t z_accel;
  uint16_t x_rot; 
  uint16_t y_rot;
  uint16_t z_rot; 
};

struct presDataPayload_t {
  uint32_t us_time;
  uint16_t unix_time;
  uint16_t fore_out_pres;
  uint16_t fore_in_pres;
  uint16_t mid_pres;
  uint16_t rear_pres; 
};

uint8_t test_data[20] = {0}; 

void setupGDS(void);
void startAdv(void); 
void gds_dcp_write_callback(uint16_t conn_hdl, BLECharacteristic* chr, uint8_t* data, uint16_t len); 
bool gds_dcp_start_data_transfer(); 
bool gds_dcp_pause_data_transfer(); 
void connect_callback(uint16_t conn_handle); 
void connection_secured_callback(uint16_t conn_handle); 
void disconnect_callback(uint16_t conn_handle, uint8_t reason); 
void cccd_callback(uint16_t conn_hdl, BLECharacteristic* chr, uint16_t cccd_value); 
void setupHRM(void); 

void ble_gds_setup()
{
  if (!Serial) {
      Serial.begin(115200);
  }
  while ( !Serial ) delay(10);   // for nrf52840 with native usb

  // Initialise the Bluefruit module
  Serial.println("Initialise the Bluefruit nRF52 module");
  Bluefruit.begin();

  // Set the connect/disconnect callback handlers
  Bluefruit.Periph.setConnectCallback(connect_callback);
  Bluefruit.Periph.setDisconnectCallback(disconnect_callback);
  
  // Set connection secured callback, invoked when connection is encrypted
  Bluefruit.Security.setSecuredCallback(connection_secured_callback);

  // Configure and Start the Device Information Service
  Serial.println("Configuring the Device Information Service");
  bledis.setManufacturer("Adafruit Industries");
  bledis.setModel("Bluefruit Feather52");
  bledis.begin();

  // Start the BLE Battery Service and set it to 100%
  Serial.println("Configuring the Battery Service");
  blebas.begin();
  blebas.write(100);

  //Setup Gait Data Service
  available_data_len = 400; //TODO: temp measure 
  Serial.println("Configuring Gait Data Service"); 
  setupGDS(); 
  
  // Setup the Heart Rate Monitor service using
  // BLEService and BLECharacteristic classes
  Serial.println("Configuring the Heart Rate Monitor Service");
  setupHRM();
  
  // Setup the advertising packet(s)
  Serial.println("Setting up the advertising payload(s)");
  // startAdv();

  Serial.println("Ready Player One!!!");
  Serial.println("\nAdvertising");
}

void startAdv(void)
{
  // Advertising packet
  BLEConnection* conn = Bluefruit.Connection(0);
  if (!Bluefruit.Advertising.isRunning() && !(conn && conn->connected())) {
    Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
    Bluefruit.Advertising.addTxPower();

    // Include HRM Service UUID
    Bluefruit.Advertising.addService(gds);

    // Include Name
    Bluefruit.ScanResponse.addName();
    
    /* Start Advertising
    * - Enable auto advertising if disconnected
    * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
    * - Timeout for fast mode is 30 seconds
    * - Start(timeout) with timeout = 0 will advertise forever (until connected)
    * 
    * For recommended advertising interval
    * https://developer.apple.com/library/content/qa/qa1931/_index.html   
    */
    Bluefruit.Advertising.restartOnDisconnect(true);
    Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
    Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
    Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds  
  }
}

void setupGDS(void)
{
  gds.begin();
  
  gds_dcp.setProperties(CHR_PROPS_NOTIFY);
  gds_dcp.setProperties(CHR_PROPS_WRITE); 
  gds_dcp.setPermission(SECMODE_ENC_NO_MITM, SECMODE_ENC_NO_MITM); 
  gds_dcp.setFixedLen(GDS_DCP_LEN); 
  gds_dcp.begin(); 
  uint8_t default_retcode = GDS_DCP_OP_DEFAULT_RETCODE; 
  gds_dcp.write(&default_retcode, GDS_DCP_LEN); 
  gds_dcp.setWriteCallback(gds_dcp_write_callback); 

  gds_adb.setProperties(CHR_PROPS_READ);
  gds_adb.setPermission(SECMODE_ENC_NO_MITM, SECMODE_NO_ACCESS); 
  gds_adb.setFixedLen(GDS_ADB_LEN); 
  gds_adb.begin(); 
  gds_adb.write(&available_data_len, GDS_ADB_LEN); 
  
  gds_data.setProperties(CHR_PROPS_NOTIFY);
  gds_data.setPermission(SECMODE_ENC_NO_MITM, SECMODE_NO_ACCESS); 
  gds_data.setMaxLen(MAX_GDS_DATA_LEN); 
  gds_data.begin(); 
  uint8_t gds_dummy_data [MAX_GDS_DATA_LEN] = {0}; 
  gds_data.write(gds_dummy_data, MAX_GDS_DATA_LEN); 
  gds_data.setCccdWriteCallback(cccd_callback); 
}

void gds_dcp_write_callback(uint16_t conn_hdl, BLECharacteristic* chr, uint8_t* data, uint16_t len)
{
  if (len == 1) {
    uint8_t dcp_write_result = GDS_DCP_OP_FAIL_RETCODE; 
    switch(data[0]) {
      case GDS_DCP_START_TRANSFER_OPCODE:
        Serial.println("Peer requested start data transfer."); 
        if (gds_dcp_start_data_transfer()) {
          dcp_write_result = GDS_DCP_OP_SUCCESS_RETCODE; 
        }
        else {
          dcp_write_result = GDS_DCP_OP_FAIL_RETCODE; 
        }
        break;
      case GDS_DCP_PAUSE_TRANSFER_OPCODE: 
        Serial.println("Peer requested pause data transfer."); 
        if (gds_dcp_pause_data_transfer()) {
          dcp_write_result = GDS_DCP_OP_SUCCESS_RETCODE; 
        }
        else {
          dcp_write_result = GDS_DCP_OP_FAIL_RETCODE; 
        }
        break; 
      default:
        dcp_write_result = GDS_DCP_OP_FAIL_RETCODE; 
        Serial.println("Peer requested unrecognized DCP opcode"); 
        break;
    }
    gds_dcp.notify(&dcp_write_result, sizeof(dcp_write_result)); 
  } 
}

bool gds_dcp_start_data_transfer()
{
  begin_data_transfer(0); 
  return true; 
  // bool retcode = false; 
  // if (available_data_len > 0) {
  //   transfer_data = true; 
  //   retcode = true; 
  // }
  // else {
  //   transfer_data = false; 
  //   retcode = false; 
  // }
  // return retcode; 
}

bool gds_dcp_pause_data_transfer()
{
  return true; 
}

void setupHRM(void)
{
  // Configure the Heart Rate Monitor service
  // See: https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.service.heart_rate.xml
  // Supported Characteristics:
  // Name                         UUID    Requirement Properties
  // ---------------------------- ------  ----------- ----------
  // Heart Rate Measurement       0x2A37  Mandatory   Notify
  // Body Sensor Location         0x2A38  Optional    Read
  // Heart Rate Control Point     0x2A39  Conditional Write       <-- Not used here
  hrms.begin();

  // Note: You must call .begin() on the BLEService before calling .begin() on
  // any characteristic(s) within that service definition.. Calling .begin() on
  // a BLECharacteristic will cause it to be added to the last BLEService that
  // was 'begin()'ed!

  // Configure the Heart Rate Measurement characteristic
  // See: https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.heart_rate_measurement.xml
  // Properties = Notify
  // Min Len    = 1
  // Max Len    = 8
  //    B0      = UINT8  - Flag (MANDATORY)
  //      b5:7  = Reserved
  //      b4    = RR-Internal (0 = Not present, 1 = Present)
  //      b3    = Energy expended status (0 = Not present, 1 = Present)
  //      b1:2  = Sensor contact status (0+1 = Not supported, 2 = Supported but contact not detected, 3 = Supported and detected)
  //      b0    = Value format (0 = UINT8, 1 = UINT16)
  //    B1      = UINT8  - 8-bit heart rate measurement value in BPM
  //    B2:3    = UINT16 - 16-bit heart rate measurement value in BPM
  //    B4:5    = UINT16 - Energy expended in joules
  //    B6:7    = UINT16 - RR Internal (1/1024 second resolution)
  hrmc.setProperties(CHR_PROPS_NOTIFY);
  hrmc.setPermission(SECMODE_ENC_NO_MITM, SECMODE_NO_ACCESS);
  hrmc.setFixedLen(2);
  hrmc.setCccdWriteCallback(cccd_callback);  // Optionally capture CCCD updates
  hrmc.begin();
  uint8_t hrmdata[2] = { 0b00000110, 0x40 }; // Set the characteristic to use 8-bit values, with the sensor connected and detected
  hrmc.write(hrmdata, 2);

  // Configure the Body Sensor Location characteristic
  // See: https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.body_sensor_location.xml
  // Properties = Read
  // Min Len    = 1
  // Max Len    = 1
  //    B0      = UINT8 - Body Sensor Location
  //      0     = Other
  //      1     = Chest
  //      2     = Wrist
  //      3     = Finger
  //      4     = Hand
  //      5     = Ear Lobe
  //      6     = Foot
  //      7:255 = Reserved
  bslc.setProperties(CHR_PROPS_READ);
  bslc.setPermission(SECMODE_ENC_NO_MITM, SECMODE_NO_ACCESS);
  bslc.setFixedLen(1);
  bslc.begin();
  bslc.write8(2);    // Set the characteristic to 'Wrist' (2)
}

void connect_callback(uint16_t conn_handle)
{
  // Get the reference to current connection
  BLEConnection* connection = Bluefruit.Connection(conn_handle);

  char central_name[32] = { 0 };
  connection->getPeerName(central_name, sizeof(central_name));

  Serial.print("Connected to ");
  Serial.println(central_name);

  Serial.println("Attempting to PAIR with the central device, please press PAIR on your phone ... ");
  connection->requestPairing();
  Serial.print("conn interval: "); 
  Serial.println(connection->getConnectionInterval());
  connection->requestConnectionParameter(8, BLE_GAP_CONN_SLAVE_LATENCY, BLE_GAP_CONN_SUPERVISION_TIMEOUT_MS/10); 
  delay(10); 
}

void connection_secured_callback(uint16_t conn_handle)
{
  BLEConnection* conn = Bluefruit.Connection(conn_handle);

  if ( !conn->secured() )
  {
    // It is possible that connection is still not secured by this time.
    // This happens (central only) when we try to encrypt connection using stored bond keys
    // but peer reject it (probably it remove its stored key).
    // Therefore we will request an pairing again --> callback again when encrypted
    conn->requestPairing();
  }
  else
  {
    Serial.println("Secured");
  }
}

/**
 * Callback invoked when a connection is dropped
 * @param conn_handle connection where this event happens
 * @param reason is a BLE_HCI_STATUS_CODE which can be found in ble_hci.h
 */
void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;

  Serial.print("Disconnected, reason = 0x"); Serial.println(reason, HEX);
  Serial.println("Advertising!");
}

void cccd_callback(uint16_t conn_hdl, BLECharacteristic* chr, uint16_t cccd_value)
{
    // Display the raw request packet
    Serial.print("CCCD Updated: ");
    //Serial.printBuffer(request->data, request->len);
    Serial.print(cccd_value);
    Serial.println("");

    // Check the characteristic this CCCD update is associated with in case
    // this handler is used for multiple CCCD records.
    if (chr->uuid == hrmc.uuid) {
        if (chr->notifyEnabled(conn_hdl)) {
            Serial.println("Heart Rate Measurement 'Notify' enabled");
        } else {
            Serial.println("Heart Rate Measurement 'Notify' disabled");
        }
    } 
    if (chr->uuid == gds.uuid) {
      if (chr->notifyEnabled(conn_hdl)) {
        Serial.println("GDS notify enabled"); 
      } else {
        Serial.println("GDS notify disabled"); 
      }
    }
}

void ble_begin_data_transfer() {
  startAdv(); 
}

void begin_data_transfer(uint16_t conn_handle)
{
    File root = SD.open("/");
    File entry =  root.openNextFile();
    // while (entry && entry.isDirectory()) {
    //   root.close(); 
    //   root = entry;
    //   entry = root.openNextFile(); 
    // }
    uint16_t sent_cnt = 0; 
    uint8_t buffer[20] = {0}; 
    while (entry) {
      if (!entry.isDirectory()) {
        BLEConnection* conn = Bluefruit.Connection(conn_handle);
        if (conn && conn->connected() && conn->secured()) {
          Serial.print(entry.name());
          Serial.print("\t\t Entry size: ");
          Serial.println(entry.size(), DEC);
          Serial.print("\t\t Entry pos: ");
          Serial.println(entry.position(), DEC); 
          while (entry.position() < entry.size()) {
            if (conn && conn->connected() && conn->secured()) {
              entry.read(&buffer, sizeof(buffer)); 
              // for (uint8_t i = 0; i < 20; i ++) {
              //   Serial.print(buffer[i], HEX);
              //   Serial.print(" "); 
              // }
              if (!gds_data.notify(&buffer, sizeof(buffer))) {
                Serial.println("ERROR: Can't notify gds data");  
              } else{
                sent_cnt ++; 
              }

              // if (sent_cnt == 60) {
              //   entry.close();
              //   root.close(); 
              //   return; 
              // }

              // Serial.println("");
            } else {
              break; 
            }
          }
          String entryName = entry.name(); 
          bool close = false;
          close = entry.position() == entry.size(); 
          entry.close();
          if (close) {
            SD.remove(entryName); 
            Serial.print("Removed file: ");
            Serial.println(entryName);  
          }
          Serial.println("closed file"); 
          entry =  root.openNextFile();
          Serial.println("opened next file"); 
        } else {
          Serial.println("No connetion, start adv."); 
          startAdv();
          break; 
        }
      } else {
        entry.close(); 
        entry = root.openNextFile(); 
      }

    }
    entry.close();
    // sd__print_files(root, 0);
    root.close();

}

void ble_stop_data_transfer()
{
  if (Bluefruit.Advertising.isRunning()) {
    Bluefruit.Advertising.stop(); 
  }
  uint16_t const conn_handle = 0;
  BLEConnection* conn = Bluefruit.Connection(conn_handle);
  if (conn && conn->connected()) {
    conn->disconnect(); 
  }
}

void ble_gds_loop()
{
  digitalToggle(LED_RED);
  uint16_t const conn_handle = 0;
  BLEConnection* conn = Bluefruit.Connection(conn_handle);
//   if (conn && conn->connected() && conn->secured()) {
//     uint8_t hrmdata[2] = { 0b00000110, bps++ };           // Sensor connected, increment BPS value
    
//     // Note: We use .notify instead of .write!
//     // If it is connected but CCCD is not enabled
//     // The characteristic's value is still updated although notification is not sent
// //    if ( hrmc.notify(hrmdata, sizeof(hrmdata)) ){
// //      Serial.print("Heart Rate Measurement updated to: "); Serial.println(bps); 
// //    }else{
// //      Serial.println("ERROR: Notify not set in the CCCD or not connected!");
// //    }

//     // uint8_t test_data = 0; 
//     test_data[0] = 0; 
//     // begin_data_transfer(conn_handle); 
//     while (transfer_data && available_data_len > 0) {
//       // imuDataPayload_t imuDummy; 
//       // imuDummy.us_time = (uint16_t) test_data; 
//       // imuDummy.unix_time = (uint32_t) test_data;
//       // imuDummy.x_accel = (uint16_t) test_data; 
//       // imuDummy.y_accel = (uint16_t) test_data; 
//       // imuDummy.z_accel = (uint16_t) test_data; 
//       // imuDummy.x_rot = (uint16_t) test_data; 
//       // imuDummy.y_rot = (uint16_t) test_data; 
//       // imuDummy.z_rot = (uint16_t) test_data;

// //      Serial.print("imu dummy size"); 
// //      Serial.println(sizeof(imuDummy)); 
      
//       // if (gds_data.notify(&test_data, sizeof(test_data))) {
//       //   Serial.println("transferring data");  
//       // }
//       // else {
//       //   Serial.println("ERROR: Can't notify gds data");
//       // }

//       if (!gds_data.notify(&test_data, sizeof(test_data))) {
//         Serial.println("ERROR: Can't notify gds data");  
//       }

//       // presDataPayload_t presDummy;
//       // presDummy.us_time = (uint16_t) test_data; 
//       // presDummy.unix_time = (uint32_t) test_data;
//       // presDummy.fore_out_pres = (uint16_t) test_data; 
//       // presDummy.fore_in_pres = (uint16_t) test_data; 
//       // presDummy.mid_pres = (uint16_t) test_data; 
//       // presDummy.rear_pres = (uint16_t) test_data; 

// //      Serial.print("pres dummy size"); 
// //      Serial.println(sizeof(presDummy));
      
//       // if (gds_data.notify(&presDummy, sizeof(presDummy))) {
//       //   Serial.println("transferring pres data");  
//       // }
//       // else {
//       //   Serial.println("ERROR: Can't notify pres gds data");
//       // }

//       // test_data++; 
//       test_data[0] ++; 
//       available_data_len --; 
//       if (available_data_len % 4 == 0) {
//         // BLEConnection* connection = Bluefruit.Connection(conn_handle);
//         // Serial.println(connection->getConnectionInterval());
//         delay(1); //TODO: test for now  
//       }
//     }
//     transfer_data = false; 
//     available_data_len = 400; 
//   }

  // Only send update once per second
  // delay(10);
}
