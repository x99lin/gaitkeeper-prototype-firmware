/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/

#include <Arduino.h>
#include <Adafruit_TinyUSB.h>

#include "tekscan.h"
#include "../lib/defines.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

static tekscan_data_s tekscan_data;
static tekscan_compressed_data_s tekscan_compresed_data;

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/

void tekscan__setup(void) {
    Serial.println("================ TEKSCAN SETUP BEGIN ================");
  
    pinMode(FOREFOOT_LEFT_ADC_PIN, INPUT);
    pinMode(FOREFOOT_RIGHT_ADC_PIN, INPUT);
    pinMode(MIDFOOT_ADC_PIN, INPUT);
    pinMode(HEEL_ADC_PIN, INPUT);

    analogReference(ADC_REFERENCE); // 3.6V internal reference
    analogReadResolution(ADC_RESOLUTION);

    Serial.println("===================================================");
    Serial.println("");
}

void tekscan__serial_debug(void) {
    int forefoot_left_adc_value = analogRead(FOREFOOT_LEFT_ADC_PIN);
    int forefoot_right_adc_value = analogRead(FOREFOOT_RIGHT_ADC_PIN);
    int midfoot_adc_value = analogRead(MIDFOOT_ADC_PIN);
    int heel_adc_value = analogRead(HEEL_ADC_PIN);

    Serial.println("Tekscan ADC values as ratio to reference voltage");
    Serial.print((float)forefoot_left_adc_value / (float)ADC_RAW_MAX);
    Serial.print("\t");
    Serial.print((float)forefoot_right_adc_value / (float)ADC_RAW_MAX);
    Serial.println("");
    Serial.print("    ");
    Serial.print((float)midfoot_adc_value / (float)ADC_RAW_MAX);
    Serial.println("");
    Serial.print("    ");
    Serial.print((float)heel_adc_value / (float)ADC_RAW_MAX);

    Serial.println("");
    Serial.println("");
}

tekscan_compressed_data_s *tekscan__get_data(void) {
    // tekscan_compresed_data.is_quiet = true;
    tekscan_compresed_data.is_quiet = false;

    tekscan_data.forefoot_left = analogRead(FOREFOOT_LEFT_ADC_PIN);
    // if (tekscan_data.forefoot_left > NO_PRESSURE_ADC_THRESHOLD) {
    //     tekscan_compresed_data.is_quiet = false;
    // }
    tekscan_data.forefoot_right = analogRead(FOREFOOT_RIGHT_ADC_PIN);
    // if (tekscan_data.forefoot_right > NO_PRESSURE_ADC_THRESHOLD) {
    //     tekscan_compresed_data.is_quiet = false;
    // }
    tekscan_data.midfoot = analogRead(MIDFOOT_ADC_PIN);
    // if (tekscan_data.midfoot > NO_PRESSURE_ADC_THRESHOLD) {
    //     tekscan_compresed_data.is_quiet = false;
    // }
    tekscan_data.heel = analogRead(HEEL_ADC_PIN);
    // if (tekscan_data.heel > NO_PRESSURE_ADC_THRESHOLD) {
    //     tekscan_compresed_data.is_quiet = false;
    // }

    tekscan_compresed_data.data[0] = tekscan_data.forefoot_left;
    tekscan_compresed_data.data[1] = (tekscan_data.forefoot_left) >> 8;
    tekscan_compresed_data.data[1] &= 0x3f;

    tekscan_compresed_data.data[2] = tekscan_data.forefoot_right;
    tekscan_compresed_data.data[3] = (tekscan_data.forefoot_right) >> 8;
    tekscan_compresed_data.data[3] &= 0x3f;

    tekscan_compresed_data.data[4] = tekscan_data.midfoot;
    tekscan_compresed_data.data[5] = (tekscan_data.midfoot) >> 8;
    tekscan_compresed_data.data[5] &= 0x3f;

    tekscan_compresed_data.data[6] = tekscan_data.heel;
    tekscan_compresed_data.data[1] |= (((tekscan_data.heel >> 8) & 0x03) << 6);
    tekscan_compresed_data.data[3] |= (((tekscan_data.heel >> 10) & 0x03) << 6);
    tekscan_compresed_data.data[5] |= (((tekscan_data.heel >> 12) & 0x03) << 6);

    return &tekscan_compresed_data;
}
