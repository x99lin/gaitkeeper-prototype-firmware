#ifndef TEKSCAN_H
#define TEKSCAN_H

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

#define FOREFOOT_LEFT_ADC_PIN       A1
#define FOREFOOT_RIGHT_ADC_PIN      A2
#define MIDFOOT_ADC_PIN             A3
#define HEEL_ADC_PIN                A4

#define ADC_RAW_MAX                 16384U

#define NO_PRESSURE_ADC_THRESHOLD   100U

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

typedef struct tekscan_data_s {
    uint16_t forefoot_left;
    uint16_t forefoot_right;
    uint16_t midfoot;
    uint16_t heel;
} tekscan_data_s;

typedef struct tekscan_compressed_data_s {
    uint8_t data[7];
    bool is_quiet;
} tekscan_compressed_data_s;

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

void tekscan__setup(void);

void tekscan__serial_debug(void);

tekscan_compressed_data_s *tekscan__get_data(void);

#endif // TEKSCAN_H
