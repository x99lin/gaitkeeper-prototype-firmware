#ifndef SD_H
#define SD_H

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/

#include <stdint.h>

#include "../rtc/rtc.h"
#include "../imu/imu.h"
#include "../tekscan/tekscan.h"
#include "../lib/defines.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

typedef struct package_s {
    uint8_t reduced_unixtime[3];
    uint8_t wrapped_millis;
    uint16_t imu_accel[3];
    uint16_t imu_rot[3];
    uint16_t pressure[4];
} package_s;

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

void sd__setup(void);

void sd__serial_debug(void);

void sd__run(void);

#endif // SD_H
