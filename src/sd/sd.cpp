/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/

#include <Arduino.h>
#include <Adafruit_TinyUSB.h>
#include <SPI.h>
#include <SD.h>

#include "sd.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

#define PACKAGE_LENGTH      20U
#define NUM_PACKAGES        1000U //1500U
#define BUFFER_LENGTH       ((PACKAGE_LENGTH) * (NUM_PACKAGES))

#define CONSECUTIVE_QUIET_FRAMES_THRESHOLD      (NUM_PACKAGES)

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

typedef enum datalogging_state_e {
    STATE_LOGGING = 0,
    STATE_IDLE,
} datalogging_state_e;

typedef struct datalogging_state_s {
    datalogging_state_e state;
} datalogging_state_s;

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

static uint8_t buffer[BUFFER_LENGTH];

datalogging_state_s datalogging_state;

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

static void sd__print_files(File dir, int num_tabs) {
    while (true) {
  
        File entry =  dir.openNextFile();
        if (! entry) {
            // no more files
            break;
        }
        for (uint8_t i = 0; i < num_tabs; i++) {
            Serial.print('\t');
        }
        Serial.print(entry.name());
        if (entry.isDirectory()) {
            Serial.println("/");
            sd__print_files(entry, num_tabs + 1);
        } else {
            // files have sizes, directories do not
            Serial.print("\t\t");
            Serial.println(entry.size(), DEC);
        }
        entry.close();
    }
}

static bool sd__self_test(void) {

    // Remove test file if it exists
    SD.remove("test.txt");

    // Write to a test file
    File test_file = SD.open("test.txt", FILE_WRITE);
    
    if (test_file) {
        test_file.println("A");
        test_file.close();
    } else {
        Serial.println("SD self test failed, unable to write to test file");
        return false;
    }

    // Read from test file
    File test_file2 = SD.open("test.txt", FILE_READ);
    if (test_file2) {
        char test_file_content = test_file2.read();
        if (test_file_content != 'A') {
            Serial.println("SD self test failed, test file contents do not match written value");
        }

        Serial.println("Self test passes");
        test_file2.close();
    } else {
        Serial.println("SD self test failed, unable to read from test file");
        return false;
    }
    SD.remove("test.txt");

    return true;
}

static bool sd__write_buffered_data(uint8_t *data, int length) {

    // Make filename based on RTC datetime
    DateTime *now = rtc__get_raw_data();

    String filename = "";
    filename.concat(2000 - now->year());
    filename = filename.substring(filename.length() - 2);
    if (now->month() < 10) {
        filename.concat(0);
    }
    filename.concat(now->month());
    if (now->day() < 10) {
        filename.concat(0);
    }
    filename.concat(now->day());
    if (now->hour() < 10) {
        filename.concat(0);
    }
    filename.concat(now->hour());
    char filename_char_array[9];
    filename.toCharArray(filename_char_array, 9);

    File writefile = SD.open(filename_char_array, FILE_WRITE);
    if (!writefile) {
        Serial.println("SD: Failure to write data");
        return false;
    }

    int bytes_written = writefile.write(data, length);
    if (bytes_written != BUFFER_LENGTH) {
        Serial.println("SD: Didn't write expected size");
        return false;
    }

    writefile.close();

    return true;
}

static void sd__fill_buffer_blocking(void) {

    // timestamp frame
    for (size_t i = 0; i < 16; i++) {
        buffer[i] = 0xff;
    }

    const uint32_t unixtime = rtc__get_unix_timestamp();
    buffer[16] = (unixtime);
    buffer[17] = (unixtime >> 8);
    buffer[18] = (unixtime >> 16);
    buffer[19] = (unixtime >> 24);    

    // data frames
    bool is_buffer_quiet = true;
    for (size_t buffer_item = 1; buffer_item < NUM_PACKAGES; buffer_item++) {
        
        buffer[buffer_item * PACKAGE_LENGTH] = millis() % 0xff;

        const imu_data_s *imu_data = imu__get_data();
        buffer[buffer_item * PACKAGE_LENGTH + 1] = imu_data->acc_x;
        buffer[buffer_item * PACKAGE_LENGTH + 2] = (imu_data->acc_x) >> 8;
        buffer[buffer_item * PACKAGE_LENGTH + 3] = imu_data->acc_y;
        buffer[buffer_item * PACKAGE_LENGTH + 4] = (imu_data->acc_y) >> 8;
        buffer[buffer_item * PACKAGE_LENGTH + 5] = imu_data->acc_z;
        buffer[buffer_item * PACKAGE_LENGTH + 6] = (imu_data->acc_z) >> 8;
        
        buffer[buffer_item * PACKAGE_LENGTH + 7] = imu_data->gyro_x;
        buffer[buffer_item * PACKAGE_LENGTH + 8] = (imu_data->gyro_x) >> 8;
        buffer[buffer_item * PACKAGE_LENGTH + 9] = imu_data->gyro_y;
        buffer[buffer_item * PACKAGE_LENGTH + 10] = (imu_data->gyro_y) >> 8;
        buffer[buffer_item * PACKAGE_LENGTH + 11] = imu_data->gyro_z;
        buffer[buffer_item * PACKAGE_LENGTH + 12] = (imu_data->gyro_z) >> 8;

        const tekscan_compressed_data_s *tekscan_data = tekscan__get_data();
        buffer[buffer_item * PACKAGE_LENGTH + 13] = tekscan_data->data[0];
        buffer[buffer_item * PACKAGE_LENGTH + 14] = tekscan_data->data[1];
        buffer[buffer_item * PACKAGE_LENGTH + 15] = tekscan_data->data[2];
        buffer[buffer_item * PACKAGE_LENGTH + 16] = tekscan_data->data[3];
        buffer[buffer_item * PACKAGE_LENGTH + 17] = tekscan_data->data[4];
        buffer[buffer_item * PACKAGE_LENGTH + 18] = tekscan_data->data[5];
        buffer[buffer_item * PACKAGE_LENGTH + 19] = tekscan_data->data[6];

        if (tekscan_data->is_quiet == false) {
            is_buffer_quiet = false;
        }

        // experimentally determined to give ~5ms period
        delay(2);
    }

    if (is_buffer_quiet == true) {
        datalogging_state.state = STATE_IDLE;
    } else {
        datalogging_state.state = STATE_LOGGING;
    }
}

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/

void sd__setup(void) {

    Serial.println("================ SD SETUP BEGIN ================");
  
    if (!SD.begin(11)) {
      Serial.println("SD Card Initialization Failed");
      while (1);
    }

    File root = SD.open("/");
    sd__print_files(root, 0);
    root.close();

#if SD_SELF_TEST == 1
    if (sd__self_test() == false) {
        Serial.println("SD self test failed");
        while (1) {}
    }
#endif

    datalogging_state.state = STATE_LOGGING;

    Serial.println("==============================================");
    Serial.println("");
}

void sd__serial_debug(void) {

    File root = SD.open("/");
    sd__print_files(root, 0);
    root.close();
}

void sd__run(void) {

    sd__fill_buffer_blocking();

    if (datalogging_state.state == STATE_LOGGING) {
        sd__write_buffered_data(buffer, BUFFER_LENGTH);
    }
}
