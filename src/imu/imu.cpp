/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/

#include <Arduino.h>
#include <Adafruit_TinyUSB.h>
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>

#include "imu.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

static Adafruit_MPU6050 mpu6050;
static imu_data_s imu_data;

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/

void imu__setup(void) {

    Serial.println("================ IMU SETUP BEGIN ================");

    if (!mpu6050.begin(MPU6050_I2C_ADDR)) {
        Serial.println("Failed to find MPU6050 IMU");
        led__flash_forever(100);
    }
    Serial.println("MPU6050 Found!");

    mpu6050.setAccelerometerRange(MPU6050_RANGE_8_G);
    Serial.print("Accelerometer range set to: ");
    switch (mpu6050.getAccelerometerRange()) {
    case MPU6050_RANGE_2_G:
      Serial.println("+-2G");
      break;
    case MPU6050_RANGE_4_G:
      Serial.println("+-4G");
      break;
    case MPU6050_RANGE_8_G:
      Serial.println("+-8G");
      break;
    case MPU6050_RANGE_16_G:
      Serial.println("+-16G");
      break;
    }
    mpu6050.setGyroRange(MPU6050_RANGE_500_DEG);
    Serial.print("Gyro range set to: ");
    switch (mpu6050.getGyroRange()) {
    case MPU6050_RANGE_250_DEG:
      Serial.println("+- 250 deg/s");
      break;
    case MPU6050_RANGE_500_DEG:
      Serial.println("+- 500 deg/s");
      break;
    case MPU6050_RANGE_1000_DEG:
      Serial.println("+- 1000 deg/s");
      break;
    case MPU6050_RANGE_2000_DEG:
      Serial.println("+- 2000 deg/s");
      break;
    }
  
    mpu6050.setFilterBandwidth(MPU6050_BAND_21_HZ);
    Serial.print("Filter bandwidth set to: ");
    switch (mpu6050.getFilterBandwidth()) {
    case MPU6050_BAND_260_HZ:
      Serial.println("260 Hz");
      break;
    case MPU6050_BAND_184_HZ:
      Serial.println("184 Hz");
      break;
    case MPU6050_BAND_94_HZ:
      Serial.println("94 Hz");
      break;
    case MPU6050_BAND_44_HZ:
      Serial.println("44 Hz");
      break;
    case MPU6050_BAND_21_HZ:
      Serial.println("21 Hz");
      break;
    case MPU6050_BAND_10_HZ:
      Serial.println("10 Hz");
      break;
    case MPU6050_BAND_5_HZ:
      Serial.println("5 Hz");
      break;
    }
    delay(100);

    Serial.println("===============================================");
    Serial.println("");
}

void imu__serial_debug(void) {
    /* Get new sensor events with the readings */
    sensors_event_t a, g, temp;
    mpu6050.getEvent(&a, &g, &temp);
  
    /* Print out the values */
    Serial.print("Acceleration X: ");
    Serial.print(a.acceleration.x);
    Serial.print(", Y: ");
    Serial.print(a.acceleration.y);
    Serial.print(", Z: ");
    Serial.print(a.acceleration.z);
    Serial.println(" m/s^2");
  
    Serial.print("Rotation X: ");
    Serial.print(g.gyro.x);
    Serial.print(", Y: ");
    Serial.print(g.gyro.y);
    Serial.print(", Z: ");
    Serial.print(g.gyro.z);
    Serial.println(" rad/s");
  
    Serial.print("Temperature: ");
    Serial.print(temp.temperature);
    Serial.println(" degC");
  
    Serial.println("");
}

imu_data_s *imu__get_data(void) {
    sensors_event_t a, g, temp;
    mpu6050.getEvent(&a, &g, &temp);

    const float max_g = 8.0 * EARTH_GRAVITY;
    const float max_rad_per_sec = 8.72665;

    imu_data.acc_x = ((a.acceleration.x + max_g) / CONSTANT_16G_METERS_PER_SECOND_SQUARED) * pow(2, 16);
    imu_data.acc_y = ((a.acceleration.y + max_g) / CONSTANT_16G_METERS_PER_SECOND_SQUARED) * pow(2, 16);
    imu_data.acc_z = ((a.acceleration.z + max_g) / CONSTANT_16G_METERS_PER_SECOND_SQUARED) * pow(2, 16);

    imu_data.gyro_x = ((g.gyro.x + max_rad_per_sec) / CONSTANT_1000_DEGREES_PER_SECOND_IN_RAD_PER_SECOND) * pow(2, 16);
    imu_data.gyro_y = ((g.gyro.y + max_rad_per_sec) / CONSTANT_1000_DEGREES_PER_SECOND_IN_RAD_PER_SECOND) * pow(2, 16);
    imu_data.gyro_z = ((g.gyro.z + max_rad_per_sec) / CONSTANT_1000_DEGREES_PER_SECOND_IN_RAD_PER_SECOND) * pow(2, 16);

    return &imu_data;
}
