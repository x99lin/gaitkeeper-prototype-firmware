#ifndef DEFINES_H
#define DEFINES_H

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/

#include <Arduino.h>
#include <Adafruit_TinyUSB.h>

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

#define ADC_REFERENCE           (AR_INTERNAL)
#define ADC_RESOLUTION          14

#define MPU6050_I2C_ADDR        0x69 // AD0 pulled high

#define IMU_SERIAL_DEBUG        0
#define RTC_SERIAL_DEBUG        0
#define SD_SERIAL_DEBUG         0
#define TEKSCAN_SERIAL_DEBUG    0
#define BATTERY_SERIAL_DEBUG    0

#define SD_SELF_TEST            0

#endif // DEFINES_H
