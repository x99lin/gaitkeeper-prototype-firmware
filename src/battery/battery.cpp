/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/

#include <Arduino.h>
#include <Adafruit_TinyUSB.h>

#include "battery.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

static float battery__read_vbat(void) {
    float raw;

    // Set the analog reference to 3.0V (default = 3.6V)
    analogReference(AR_INTERNAL_3_0);

    // Set the resolution to 12-bit (0..4095)
    analogReadResolution(12);

    // Let the ADC settle
    delay(1);

    // Get the raw 12-bit, 0..3000mV ADC value
    raw = analogRead(VBAT_PIN);

    // Set the ADC back to the default settings
    analogReference(ADC_REFERENCE);
    analogReadResolution(ADC_RESOLUTION);

    // Let the ADC settle
    delay(1);

    // Convert the raw value to compensated mv, taking the resistor-
    // divider into account (providing the actual LIPO voltage)
    // ADC range is 0..3000mV and resolution is 12-bit (0..4095)
    return raw * REAL_VBAT_MV_PER_LSB;
}

static int battery__mv_to_percent(float mvolts) {
    if (mvolts<3300)
        return 0;

    if (mvolts <3600) {
        mvolts -= 3300;
        return mvolts/30;
    }

    mvolts -= 3600;
    return 10 + (mvolts * 0.15F );
}

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/

void battery__setup(void) {
    // Get a single ADC sample and throw it away
    battery__read_vbat();
}

int battery__get_percentage(void) {
    // Get a raw ADC reading
    float vbat_mv = battery__read_vbat();

    // Convert from raw mv to percentage (based on LIPO chemistry)
    int vbat_per = battery__mv_to_percent(vbat_mv);

    return vbat_per;
}

void battery__serial_debug(void) {
    float vbat_mv = battery__read_vbat();
    int vbat_percent = battery__get_percentage();

    Serial.print("LIPO = ");
    Serial.print(vbat_mv);
    Serial.print(" mV (");
    Serial.print(vbat_percent);
    Serial.println("%)");
}
